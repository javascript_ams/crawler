let fileExtensions =
[
  "asp",    "aspx",   "axd",    "asx",
  "asmx",   "ashx",   "cfm",    "yaws",
  "swf",    "html",   "htm",    "xhtml",
  "jhtml",  "jsp",    "jspx",   "wss",
  "do",     "action", "js",     "pl",
  "php",    "php4",   "php3",   "phtml",
  "py",     "rb",     "rhtml",  "shtml",
  "xml",    "rss",    "svg",    "cgi",
  "dll"
];

module.exports = fileExtensions;
