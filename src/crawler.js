/********* DEPENDENCIES *********/
const dns = require('dns');
const request = require('request');
const cheerio = require('cheerio');
const URL = require('url-parse');
const now = require('performance-now');
const robotto = require('robotto');
const sitemaps = require('sitemap-stream-parser');
const geoip = require('geoip-lite');
const parseDomain = require('parse-domain');
const argv = require('minimist')(process.argv.slice(2));
require('events').EventEmitter.defaultMaxListeners = 200;

const convert = require("./convert-time");
const FileExtensions = require("./fileExtensions");

/********* DATABASE *********/
const mongoose = require('mongoose');
const DBFile = require('./database');
const DB = DBFile();
mongoose.Promise = require('bluebird');
mongoose.connect("mongodb://localhost/webcrawler", {
  useMongoClient: true
});

let promise;
let promiseVisited;
/********* DEBUGGING *********/
let DEBUG = false;
if (argv["DEBUG"]) {
  DEBUG = argv["DEBUG"];
}
/********* VARIABLES *********/
let search_word = "asdfagadgdag";
let maxPagesToVist = 50;
let timeStart;
/* --Domain variables-- */
let domainsToVisit = [];
let domainsToVisitObjects = [];
let domainsVisited = new Set();
let thisDomain;
let domainId;
/* --Pages variables-- */
let pagesToVisit = [];
let pagesVisited = new Set();
let numPagesVisited = 0;
let pageStatusArray = [];
let pageStatusObject = {};
let internalLinksCounter = 0;
let externalLinksCounter = 0;
let errorsCounter = 0;
let tooManyErrors = false;
let linkCounterArray = [];
let allLinks = [];
/* --URL variables-- */
let startUrl;
let url;
let baseURL;
let host;
let hasSitemap = false;
let robotsUrl;
let geo;
let hasRobotsTxt = false;
let firstVisitOfDomain = true;
let crawlIsAllowed = false;
let nextPage;
let removeProtocol = /(http:\/\/?|https:\/\/)/;

let allFilesCounter = 0;
let filesSet = new Set();
let filesArray = [];

let checkedLinks = new Set();
let checkedDeadLinks = new Set();

function start() {
  promise = DB.getDomainPromise();
  promiseVisited = DB.getVisitedDomains();
  connectToDbAndPrepareDomainURL(); //start application
}

/**** DATABASE FUNCTIONS ****/
/* This function uses the promise from getDomainPromise to create the startUrl for the crawler, it then starts the updateCrawlTime() function to update the lastCrawled field ind the database of aformentioned startUrl, after that it will start the crawl() function*/
function connectToDbAndPrepareDomainURL() {
  promise.then(function(domains) {
    /* if there is no domain to crawl exit process */
    if (domains === null) {
      mongoose.connection.close();
      process.send({
        noDomain: "No Domains to crawl"
      });
      process.exit(0);
    }
    startUrl = domains.url;
    thisDomain = domains.url; //save domainname for later use
    domainId = domains._id; //save domain id for later use
    promiseVisited.then(domains => {
        pushVisitedDomainsToArray(domains);
      })
      .then(() => {
        pagesToVisit.push(startUrl);
        nextPage = pagesToVisit.pop();
        console.log(`Worker ${process.pid} is crawling the domain: ${nextPage}`);

        timeStart = now(); // Start timer for crawling process
        crawl(); // Start crawling
      });
  });
}


/* this function adds all the domains from the db to a set to prevent duplicate entries */
function pushVisitedDomainsToArray(domains) {
  domains.forEach(function(domain) {
    domainsVisited.add(domain.url);
  });
  return Promise.resolve();
}

/**** CRAWLING FUNCTIONS ****/
function crawl() {
  // enter when starting to crawl the domain
  if (firstVisitOfDomain) {
    // prepare url parser and baseURL for correct relative links
    url = new URL(nextPage);
    baseURL = `${url.protocol}//${url.hostname}`;
    pagesVisited.add(nextPage.replace(removeProtocol, "")); // add url to pagesVisited, for when the url is the same as the start page, no need to crawl a page 2 times
    nextPage += "/";
    // Prepare the correct robots.txt url
    if (nextPage !== undefined) {
      robotsUrl = robotto.getRobotsUrl(nextPage);
    }
    DEBUG && console.log("************************");
    DEBUG && console.log("ERSTER BESUCH DER DOMAIN");
    DEBUG && console.log("Pathname: " + url.pathname);
    DEBUG && console.log("Origin: " + url.origin);
    // check if a robotstxt file exists
    checkRobotStatus(robotsUrl);
    /* all the other pages enter here */
  } else {
    // if pagecrawling was allowed proceed to push page data into the array for later saving
    if (crawlIsAllowed) {
      pageStatusArray.push(pageStatusObject);
    }
    // reset variables
    crawlIsAllowed = false;
    pageStatusObject = {};
    // manual garbage collection, works only when the --expose-gc flag is used
    try {
      global.gc();
    } catch (e) {
      DEBUG && console.log("garbage collection isn't activated");
    }
    // proceed the crawling
    goAhead();
  }
}

/* This function checks if there is a robots.txt file. If the domain has one,
it will start the getSiteMap() function. Otherwise it will proceed with
goAhead() and the crawler gathers the data without sitemap */
function checkRobotStatus(url) {
  let options = {
    url: url,
    agentOptions: {
      rejectUnauthorized: false,
    }
  };
  request.head(options, (error, response) => {
    if (error) {
      errorsCounter++;
      hasSitemap = false;
      hasRobotsTxt = false;
      goAhead();
      return;
    }
    // if there is no response from the request
    if (response === undefined) {
      DEBUG && console.log("NO ROBOTS.TXT");
      DEBUG && console.log("************************\n");
      hasSitemap = false;
      hasRobotsTxt = false;
      goAhead();
      return;
    }
    // if there is a robots.txt
    if (response.statusCode === 200) {
      DEBUG && console.log("ROBOTS.TXT HERE");
      hasRobotsTxt = true;
      getSiteMap(url);
      return;
    }
    // if there isn't a robots.txt
    DEBUG && console.log("NO ROBOTS.TXT");
    DEBUG && console.log("************************\n");
    hasSitemap = false;
    hasRobotsTxt = false;
    goAhead();
    return;
  });
}

/* This function tries to gather sitemap information from robots.txt. If no
data can be acquired it proceeds with goAhead() */
function getSiteMap(roboUrl) {
  sitemaps.sitemapsInRobots(roboUrl, function(err, urls) {
    if (err) {
      errorsCounter++;
      hasSitemap = false;
      goAhead();
    }
    if (urls.length > 0) {
      sitemaps.parseSitemaps(urls, (url) => {
        pagesToVisit.push(url);
      }, function(err, sitemaps) {
        hasSitemap = true;
      });
    }
    goAhead();
  });
}

function goAhead() {
  /* looks for unvisited pages */
  if (numPagesVisited >= maxPagesToVist) {
    nextPage = undefined;
  } else if (errorsCounter >= 10) {
    nextPage = undefined;
    tooManyErrors = true;
  } else {
    searchNewPage();
  }
  console.log("--------------------------------");
  console.log("Worker " + process.pid + "--> Pages visited: " + numPagesVisited);
  console.log("Current Page: " + nextPage);
  console.log("--------------------------------");
  /* In case there are no more pages to visit on this domain,write infos to the DB and end process.
  Otherwise if nextPage contains an unvisited page, proceed with visitPage()*/
  if (nextPage === undefined) {
    let timeEnd = now(); // Stop the timer

    let elapsedTime = timeEnd - timeStart;
    let formattedTime = convert.timeFormat(elapsedTime);

    console.log("********************************************");
    console.log("No more pages to visit.");
    console.log("Crawled through all the content of the site.");
    console.log("Time elapsed: " + formattedTime);
    console.log("Time elapsed: " + elapsedTime / 1000);
    console.log("Pages visited: " + numPagesVisited);
    console.log("********************************************");

    finish(elapsedTime);
  } else {
    let nextPageObject = new URL(nextPage);
    DEBUG && console.log("NOT VISITED " + nextPage);
    checkTypeOfUrl(nextPageObject, "nextPage");
  }
}

/* already visited, change until there is an unvisited Page. If there are no
no more unvisited pages, exit the loop */
function searchNewPage() {
  while (nextPage !== undefined && pagesVisited.has(nextPage.replace(removeProtocol, ""))) {
    nextPage = pagesToVisit.pop();
  }
}

function checkTypeOfUrl(urlObject, uriType) {
  let goodToCrawl;
  let path;
  let url;

  path = urlObject.pathname;
  url = urlObject.href;

  if (path.includes('.')) {
    let ext;
    ext = path.split('.').pop().toLowerCase();
    goodToCrawl = FileExtensions.includes(ext);
    urlHandler(urlObject, goodToCrawl, uriType);

  } else {
    let mimeType;
    mimeType = checkMimeType(url);
    mimeType
      .then((resolveAnswer) => {
        goodToCrawl = resolveAnswer.includes("text/");
        urlHandler(urlObject, goodToCrawl, uriType);
      }, (rejectAnswer) => {
        urlHandler(urlObject, false, rejectAnswer);
      })
      .catch((err) => {
        console.log("CATCH ERROR: " + err);
        // urlHandler(urlObject, false, "error");
      });
  }
}

function checkMimeType(url) {

  return new Promise((resolve, reject) => {
    let options = {
      url: url,
      agentOptions: {
        rejectUnauthorized: false,
      }
    };
    request.head(options, (error, response) => {
      if (error) {
        console.error("Error while trying to check MimeType");
        console.log(url);
        errorsCounter++;
        reject("error");
        return;
      }
      if (response === undefined) {
        // console.error("MimeType response was undefined");
        // console.log(url);
        checkedDeadLinks.add(url);
        reject("undefined");
        return;
      }
      // console.error("MimeType response was successfull");
      // console.log(url);
      resolve(response.headers['content-type']);
    });
  });
}

function urlHandler(urlObject, goodToCrawl, uriType) {

  let url;
  let urlHost;
  let hadErrorOrNoResponse = (uriType.includes("error") || uriType.includes("undefined"));

  url = urlObject.href;
  urlHost = urlObject.hostname;

  if (!goodToCrawl && !hadErrorOrNoResponse) {
    if (url.includes("facebook") === false) {

      console.log("SAVING FILE: " + url);
      allFilesCounter++;
      filesSet.add(url);
      pagesVisited.add(url.replace(removeProtocol, ""));
    }
    if (uriType === "nextPage") {
      crawl();
    }
    return;
  }
  switch (uriType) {
    case "nextPage":
      visitPage(url, urlObject);
      break;
    case "relative":
      checkIfAlreadyVisited(url);
      break;
    case "absolute":
      if (thisDomain.includes(urlHost)) {
        internalLinksCounter++;
        checkIfAlreadyVisited(url);
      } else {
        let absoluteLink;
        externalLinksCounter++;
        absoluteLink = checkOrigin(urlHost, url); // go check the origin
      }
      break;
  }
}

/* Checks if it has permission to crawl the page when there's a robots.txt. if
there is none, it will crawl anyway*/
function visitPage(url, urlObject) {
  pagesVisited.add(url.replace(removeProtocol, "")); // add page to our set of visited pages
  if (!hasRobotsTxt) {
    crawlIsAllowed = true;
    crawlPage(url, urlObject);
    return;
  } else {
    /* Check if the crawler has permisson to crawl the page */
    robotto.canCrawl("*", url, (err, isAllowed) => {
      if (err) {
        console.error(err);
        crawl();
        return;
      }
      if (isAllowed) {
        crawlIsAllowed = true;
        crawlPage(url, urlObject);
        return;
      } else {
        console.log(`I'M NOT ALLOWED TO CRAWL: ${url}`);
        crawl();
        return;
      }
    });
  }
}

/* The actual crawling process */
function crawlPage(url, urlObject) {

  let options = {
    url: url,
    agentOptions: {
      rejectUnauthorized: false,
    }
  };
  numPagesVisited++; // add to counter if page is visited

  /* make the request */
  request.get(options, function(error, response, body) {
    if (firstVisitOfDomain) {
      let path = response.request.uri.pathname;
      let lastChar = path.slice(-1);
      path = path === '/' ? '' : lastChar === '/' ? path.slice(0, -1) : path;
      firstVisitOfDomain = false;
      baseURL = urlObject.origin + path;
      url = baseURL;
    }
    // if crawler encounters an error, save the only the url and that there was en error,
    // increment errorsCounter and proceed to next page
    if (error) {
      Object.assign(pageStatusObject, {
        page: url,
        hadError: true
      });
      errorsCounter++;
      crawl();
      return;
    }
    // whene there is no response, save the data and proceed to next page, increment errorsCounter
    if (response === undefined) {
      Object.assign(pageStatusObject, {
        page: url,
        noResponse: true
      });
      errorsCounter++;
      crawl();
      return;
    }
    // statuscode other than 200, increment errorsCounter
    if (response.statusCode >= 400) {
      errorsCounter++;
    }
    /* Check status code (200 is HTTP OK), in case the response is something
    other than 200, restart crawl() and move to next page */
    Object.assign(pageStatusObject, {
      page: url,
      status: response.statusCode,
      size: response.headers['content-length']
    });
    if (response.statusCode !== 200) {
      DEBUG && console.log('\n');
      crawl();
      return;
    }
    errorsCounter = 0; // if statusCode was 200, reset errorsCounter
    // Parse the document body
    let $ = cheerio.load(body);

    // TODO: word cloud
    let isWordFound = searchForWord($, search_word);

    if (isWordFound) {} else {
      collectInternalLinks($, urlObject);
    }
  });
}

/* This function allows the search for preset words. Can be expanded to a word cloud */
function searchForWord($, word) {
  let bodyText = $('html > body').text().toLowerCase();
  return (bodyText.indexOf(word.toLowerCase()) !== -1);
}


/* This function collects absolute and
relative links and filters already visited links. both type of links get pushed in an array */
function collectInternalLinks($, urlObject) {
  let meta_tag = $("meta[name='robots']"); //get meta robots tag from request
  let relativeLinks = $(`a[href^='/'], a[href^='?']`); //get relative links from request
  let absoluteLinks = $("a[href^='http']"); //get absolute links from request
  let imageTags = $("img");
  let noFollow = false;

  /* search for possible meta robots tags in the page, if the robot is not
  allowed to follow links, move to the next page */
  noFollow = lookForMetaTags(meta_tag, $);
  if (noFollow) {
    console.log("I'M NOT ALLOWED TO FOLLOW LINKS.");
    Object.assign(pageStatusObject, {
      noFollow: noFollow
    });
    crawl();
    return;
  }
  /* get all the image src from the page */
  getImages(imageTags, $);
  /* get all relative links from page and push it into array */
  let resolved = getRelativeLinks(relativeLinks, $, urlObject);

  /* get all absolute links from page, then iterate through the array and find
  out from which country the url originates. */
  getAbsoluteLinks(absoluteLinks, $);
  resolved.then(() => crawl());
}

/* this function checks if there is a robots meta tag, and returns true or false if there is a nofollow content*/
function lookForMetaTags(meta_tag, $) {
  let meta_content;
  meta_tag.each(function() {
    meta_content = `${$(this).attr('content')}`;
    if (meta_content.includes("nofollow")) {
      return true;
    }
  });
  return false;
}

/* this functions puts all found images in a set for further proccessing */
function getImages(imageTags, $) {
  imageTags.each(function() {
    allFilesCounter++;
    let imageContent = `${$(this).attr('src')}`;
    if (removeProtocol.test(imageContent) === false) {
      imageContent = `${baseURL}${imageContent}`;
    }
    filesSet.add(imageContent);
  });
}

/* This function collects all relative links from a page, and filters all already visited pages */
function getRelativeLinks(relativeLinks, $, urlObject) {
  return new Promise((res, rej) => {

    let allRelativeLinks = [];

    // loop throug all found links
    relativeLinks.each(function() {
      let relLink;
      let link;
      let path = urlObject.pathname;
      let origin = urlObject.origin;
      let linkObjectPromise;

      internalLinksCounter++;
      link = $(this).attr('href');

      linkObjectPromise = adaptLink(origin, path, link);
      linkObjectPromise
        .then((linkObject) => {
          relLink = `${urlObject.origin}${linkObject.newPath}${linkObject.newLink}`;
          if (relLink.includes('#')) {
            relLink = relLink.split('#').slice(0, 1).toString();
          }

          allLinks.push(relLink);
          allRelativeLinks.push(relLink);

          checkIfAlreadyVisited(relLink);
        });
    });
    Object.assign(pageStatusObject, {
      relLinks: {
        count: allRelativeLinks.length,
        links: allRelativeLinks
      }
    });
    return res();
  });
}

function adaptLink(origin, path, link) {
  return new Promise((res, rej) => {
    if (path === link) {
      res({
        newPath: path,
        newLink: ''
      });
    } else if (path.includes(link)) {
      if (FileExtensions.includes(link.split('.').pop())) {
        res({
          newPath: '',
          newLink: link
        });
      } else if (link === '/') {
        res({
          newPath: path,
          newLink: ''
        });
      }
      return {
        newPath: path,
        newLink: path.split(link).pop()
      };
    } else if (FileExtensions.includes(link.split('.').pop())) {
      res({
        newPath: '',
        newLink: link
      });
    } else if (link.includes('.')) {
      res({
        newPath: '',
        newLink: link
      });
    } else if (
      FileExtensions.includes(path.split('.').pop()) &&
      link.includes('.') === false) {
      res({
        newPath: '',
        newLink: link
      });

    } else {
      if (link.includes(path)) {
        res({
          newPath: '',
          newLink: link
        });
      } else {
        if (path.charAt(path.length - 1) === '/') {
          path = path.slice(0, -1);
        }
        let options = {
          url: `${origin}${path}${link}`,
          agentOptions: {
            rejectUnauthorized: false,
          }
        };
        request.head(options, function(error, response) {
          if (error) {
            res({
              newPath: '',
              newLink: link
            });
          }
          if (response === undefined) {
            res({
              newPath: '',
              newLink: link
            });
          } else if (response.statusCode >= 400) {
            res({
              newPath: '',
              newLink: link
            });
          } else if (response.statusCode === 200) {
            res({
              newPath: path,
              newLink: link
            });
          }
        });
      }
    }
  });
}

function checkIfAlreadyVisited(url) {
  let included = false;
  if (filesSet.size > 0) {
    for (let file of filesSet) {
      if (file.includes(url.replace(removeProtocol, ""))) {
        included = true;
        return;
      }
    }
  }

  if (pagesToVisit.includes(url) === false &&
    pagesVisited.has(url.replace(removeProtocol, "")) === false &&
    !included &&
    url !== baseURL) {
    pagesToVisit.push(url);
  }
}

/* This function collects all absolute links from a page, and passes it to the checkOrigin() function, which then after doing
its work passes variables to the pushAbsoluteLinks() function */
function getAbsoluteLinks(absoluteLinks, $) {
  let allAbsoluteLinks = [];

  absoluteLinks.each(function() { // iterate through the array
    let absUrlObject;
    let answer = {};
    let checkAnswer = false;
    let linkObject = {};
    let linkUrl;
    let included = false;

    linkUrl = `${$(this).attr('href')}`; // the current url

    allLinks.push(linkUrl);
    // if link was dead the first time, check it again
    if (checkedDeadLinks.has(linkUrl)) {
      answer = checkLink(linkUrl);
      // prevent double checking of links
    } else if (checkedLinks.has(linkUrl)) {
      answer = {};
    } else {
      // make headrequest to found links to check if they're valid
      answer = checkLink(linkUrl);
    }
    Object.assign(linkObject, answer);
    Object.assign(linkObject, {
      link: linkUrl
    });
    allAbsoluteLinks.push(linkObject);

    for (let domain of domainsToVisit) {
      if (domain.includes(linkUrl.replace(removeProtocol, ""))) {
        included = true;
        break;
      }
    }
    if (!included) {
      absUrlObject = new URL(linkUrl); // create url object
      checkTypeOfUrl(absUrlObject, "absolute");
    }

  });
  Object.assign(pageStatusObject, {
    absLinks: {
      count: allAbsoluteLinks.length,
      links: allAbsoluteLinks
    }
  });
}
/* this functions makes head requests to found links */
function checkLink(Link) {
  let options = {
    url: Link,
    agentOptions: {
      rejectUnauthorized: false,
    }
  };
  request.head(options, function(error, response) {
    checkedLinks.add(Link);
    if (error) {
      return {
        hadError: true
      };
    }
    if (response === undefined) {
      checkedDeadLinks.add(Link);
      return {
        dead: true
      };
    }
    return {};
  });
}

/* This function looks up the geo ip, and ignores the url if it is null. if it has a valid
geoip it passes its arguments to the pushAbsoluteLinks() function*/
function checkOrigin(host, linkUrl) {
  let answer;
  dns.lookup(host, (err, address, family) => {
    if (err) {
      console.error("Error while trying to lookup dns.");
      errorsCounter++;
      return "";
    }
    geo = geoip.lookup(address);
    if (geo !== null) {
      answer = pushAbsoluteLinks(linkUrl, host, address, family, geo.country);
    } else {
      answer = "";
    }
    return answer;
  });
}

/* this function checks if an absolute link has already been visited, thus avoiding absolute links that
point to the domain. already collected external links are also avoided */
function pushAbsoluteLinks(linkUrl, host, address, family, country) {
  let included = false;
  for (let domain of domainsToVisit) {
    if (domain.includes(linkUrl.replace(removeProtocol, ""))) {
      included = true;
      break;
    }
  }
  for (let domain of domainsVisited) {
    if (domain.includes(linkUrl.replace(removeProtocol, ""))) {
      included = true;
      break;
    }
  }
  if (!included &&
    pagesToVisit.includes(linkUrl) === false &&
    pagesVisited.has(linkUrl.replace(removeProtocol, "")) === false &&
    domainsVisited.has(linkUrl) === false) {
    let tld = parseDomain(linkUrl).tld;
    let domainObject = {
      host: linkUrl,
      country: country,
      tld: tld
    };
    domainsToVisit.push(linkUrl);
    domainsToVisitObjects.push(domainObject);
    return linkUrl;
  }
}
/* this function tries to save all the gathered data to the db and ends the process */
function finish(elapsedTime) {
  let saves = [];
  let visitedPromises;
  let waitPromises = [];
  linkCounter();
  visitedPromises = DB.getVisitedDomains();

  visitedPromises.then(domains => {
    pushVisitedDomainsToArray(domains);
  }).then(() => {
    filesSet.forEach(fileLink => {
      waitPromises.push(checkStatusOfFile(fileLink));
    });
    Promise.all(waitPromises).then(() => {
      let currentDate = new Date(); // save current date

      let saveObject = {
        hasRobotsTxt: hasRobotsTxt,
        hasSitemap: hasSitemap,
        numPagesVisited: numPagesVisited,
        elapsedTime: elapsedTime,
        currentDate: currentDate,
        tooManyErrors: tooManyErrors,
        pageStatusArray: pageStatusArray,
        externalLinksCounter: externalLinksCounter,
        internalLinksCounter: internalLinksCounter,
        linkCounterArray: linkCounterArray,
        allFilesCounter: allFilesCounter,
        filesCheckedCounter: filesSet.size,
        filesChecked: filesArray,
        domainId: domainId,
        thisDomain: thisDomain
      };

      DB.saveDomainInfos(saves, saveObject); // save all the page details
      saves.push(DB.saveDomains(domainsToVisitObjects, currentDate)); // Save all the gathered domains
      DB.isDomainUsed(domainId, false);
      DB.updateCrawlTime(domainId);
      Promise.all(saves)
        .then((promises) => {
          let savesCounter = 0;
          promises.forEach((promise) => {
            if (Number.isInteger(promise)) {
              savesCounter = promise;
            }
          });
          mongoose.connection.close();
          process.send({
            domainsSaved: savesCounter
          });
          process.send({
            finished: "All domains successfully saved. Ending process."
          });
          process.exit(0);
          return;
        }).catch((err) => {
          console.log(err);
          mongoose.connection.close();
        });
    });
  });
}

/* this function counts how many times a link appears on a domain */
function linkCounter() {
  while (allLinks.length != 0) {
    let linkCounterObject = {};
    let link = allLinks.pop();
    let count = 1;
    for (let i = 0; i < allLinks.length; i++) {
      if (allLinks[i] === link) {
        count++;
      }
    }
    linkCounterObject = {
      link: link,
      count: count
    };
    linkCounterArray.push(linkCounterObject);
    allLinks = allLinks.filter(function(item) {
      return item !== link;
    });

  }
}

/* this function makes head request to found images */
function checkStatusOfFile(fileLink) {
  // return a promise to assure data consistency
  return new Promise((resolve, reject) => {
    let fileObject = {};
    let options = {
      url: fileLink,
      agentOptions: {
        rejectUnauthorized: false,
      }
    };
    request.head(options, function(error, response) {
      if (error) {
        Object.assign(fileObject, {
          hadError: true
        });
      }
      // no response means the link is probably dead
      if (response === undefined) {
        Object.assign(fileObject, {
          dead: true
        });
        // if there is a response
      } else {
        // add status code to saved data
        Object.assign(fileObject, {
          status: response.statusCode
        });
        // if statuscode is 200 add dataType and size
        if (response.statusCode === 200) {
          Object.assign(fileObject, {
            dataType: response.headers['content-type'],
            size: response.headers['content-length']
          });
        }
      }
      // add link no matter the case
      Object.assign(fileObject, {
        url: fileLink
      });
      resolve(filesArray.push(fileObject));
    });
  });
}
start();
