module.exports = function DB() {
  const Domain = require('../models/domains'); // get user model
  const DomainDetail = require('../models/domain-details'); // domainDetails model
  const URL = require('url-parse');

  /* This function executes a query to the database and returns an promise object with id and host */
  function getDomainPromise() {
    let cutoff = new Date();
    // Domains which have been crawled more than 14 days ago
    cutoff.setDate(cutoff.getDate() - 14);

    let promise = Domain.findOneAndUpdate(
      {
        $and :
        [
          { $or: [ { 'country': /^CH/ }, { 'tld': /^ch/ }, { 'tld': /^swiss/ } ] },
          { $or: [ { lastCrawled: { $lt: cutoff } }, { lastCrawled: null } ] },
          { isUsed: false }
        ]
      }, { $set: { isUsed: true } }).select('url').exec();

    return promise;
  }

  /* this function gets the saved domains from the DB */
  function getVisitedDomains() {
    // we need only the host field (_id field is included by default)
    let promise = Domain.find( {}, { url: 1 } ).exec();
    return promise;
  }

  /* This function changes the isUsed field of the domain to prevent simultaneous crawling of a domain*/
  function isDomainUsed(id, statement) {
    Domain.findByIdAndUpdate(id, { isUsed: statement }).exec();
  }

  /* this function saves all the found data to the DB while crawling */
  function saveDomainInfos(saves, sObj) {

    let newDomainDetail = new DomainDetail({
      hasRobots: sObj.hasRobotsTxt,
      hasSitemap: sObj.hasSitemap,
      pagesVisited: sObj.numPagesVisited,
      timeElapsed: sObj.elapsedTime,
      created: sObj.currentDate,
      tooManyErrors: sObj.tooManyErrors,
      pageDetail: sObj.pageStatusArray,
      externalLinks: sObj.externalLinksCounter,
      internalLinks: sObj.internalLinksCounter,
      linkCount: sObj.linkCounterArray,
      files: {
        filesCounter: sObj.allFilesCounter,
        filesCheckedCounter: sObj.filesCheckedCounter,
        filesChecked: sObj.filesChecked
      },

      domain: sObj.domainId,
      domainName: sObj.thisDomain
    });

    saves.push(newDomainDetail.save());
  }

  /* this function saves the found domains */
  function saveDomains(domainsToVisitObjects, currentDate) {
    return new Promise((resolve, reject) => {
      let removeWWW = /^(www[2,3,4]?\.)/;
      let saveCounter = 0;
      saveRecursive(domainsToVisitObjects);
      function saveRecursive(domainsToVisitObjects){

        let domain = domainsToVisitObjects.pop();
        if (domain === undefined) {
          resolve(saveCounter);
          return;
        }
        let parsedUrl = new URL(domain.host);
        let parsedDomain;
        parsedDomain = parsedUrl.hostname.replace(removeWWW, "");
        // remove protocol to prevent duplicate entry error
        // try to find the actual domain in the database
        Domain.findOne({ domain: parsedDomain }, (err, success) => {
          if (err) {console.log(err);}
          else {
            // if domain isn't in the DB proceed with saving
            if (success === null) {
              // create a new domain
              let newDomain = new Domain({
                protocol: `${parsedUrl.protocol}//`,
                host: parsedUrl.hostname,
                domain: parsedDomain,
                url: parsedUrl.origin,
                country: domain.country,
                tld: domain.tld,
                created: currentDate,
                lastCrawled: null,
                isUsed: false
              });

              newDomain.save((error) => {
                if (error) {console.log(error);}
                saveCounter++;
                saveRecursive(domainsToVisitObjects);
              });
            } else {
              saveRecursive(domainsToVisitObjects);
            }
          }
        });
      }
    });
  }

  /* This function updates the lastCrawled field in the DB */
  function updateCrawlTime(id) {
    let updateDate = new Date();

    Domain.findByIdAndUpdate(id, { lastCrawled: updateDate }).exec();
  }

  return {
    getDomainPromise : getDomainPromise,
    getVisitedDomains : getVisitedDomains,
    isDomainUsed : isDomainUsed,
    saveDomainInfos : saveDomainInfos,
    saveDomains : saveDomains,
    updateCrawlTime : updateCrawlTime
  };
};
