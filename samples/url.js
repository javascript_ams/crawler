const URL = require('url-parse');

var url = new URL("https://www.andev.ch/blog.html");
var link = "https://www.andev.ch/blog.html";
let removeProtocol = /(http:\/\/?|https:\/\/)/;
let array = new Set();
array.add(link);
let link2 = "http://www.test.ch";
array.add(link2);
let link3 = "ramba";
array.add(link3);
var removed = link.replace(removeProtocol, "");
console.log("protocol : " + url.protocol);
console.log("slashes : " + url.slashes);
console.log("hostname : " + url.hostname);
console.log("pathname : " + url.pathname);
console.log("hash : " + url.hash);
console.log("href : " + url.href);
console.log("origin : " + url.origin);
console.log(removed);
console.log("\n");
let included = false;
for(let string of array) {
  if (string.includes("blog")){
    included = true;
    break;
  }
  console.log(string);
}
if (included){
  console.log("true");
}else {
  console.log("false");
}
