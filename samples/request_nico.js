const request = require('request');
const cheerio = require('cheerio');

let url = "https://www.mustersite.ch/homepage-maker/cm/?page_id=567";

request(url, function(error, response, body) {
  if (error){
    console.log(error);
  }
  let $ = cheerio.load(body);
  let viewport = $("*[class]");

  viewport.each(function() {
    let content = `${$(this).attr('class')}`;
    console.log(content);
  });
});

$.fn.inView = function(){
    if(!this.length) return false;
    var rect = this.get(0).getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );

};

$(window).on('scroll',function(){

    if( $('footer').inView() ) {
        // do cool stuff
    }

});
