const URL = require('url-parse');

let url = 'https://www.wbf.admin.ch/wbf/de/home.html';

let parseUrl = new URL(url);

console.log("Protocol: " + parseUrl.protocol);
console.log("Slashes: " + parseUrl.slashes);
console.log("Auth: " + parseUrl.auth);
console.log("Username: " + parseUrl.username);
console.log("Password: " + parseUrl.password);
console.log("Host: " + parseUrl.host);
console.log("Hostname: " + parseUrl.hostname);
console.log("Port: " + parseUrl.port);
console.log("pathname: " + parseUrl.pathname);
console.log("Query: " + parseUrl.query);
console.log("Hash: " + parseUrl.hash);
console.log("Href: " + parseUrl.href);
console.log("Origin: " + parseUrl.origin);

if (parseUrl.query) {
  console.log("true");
}
if (parseUrl.hash) {
  console.log("True");
}
