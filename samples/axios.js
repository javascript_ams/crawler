const axios = require("axios");
const cheerio = require('cheerio');


const url =
  "http://andev.ch/dsaffd";
axios
  .get(url)
  .then(response => {
      let $ = cheerio.load(response.data);
      let meta_tag = $("meta[name='robots']"); //get meta robots tag from request
      let meta_content;
      let followLinks = true;
      let relativeLinks = $("a[href^='/']");
      console.log(`Page title: ${$('title').text()}`);
      console.log(`Page status: ${response.status}`);
      console.log(`Page status: ${response.statusText}`);
      /* search for possible meta robots tags in the response. if the robot is not
      allowed to follow links, move to the next page */
      meta_tag.each(function() {
        meta_content = `${$(this).attr('content')}`;
        //console.log(`${meta_tag}\n`);
      });

      /* get all relative links from page and push it into array */
      relativeLinks.each(function() {
        link_url = `${$(this).attr('href')}`;
        console.log(`${$(this)}\n`);
        //console.log(`${link_url}\n`);
      });
  })
  .catch(error => {
    if (error.response) {
      let $ = cheerio.load(error.response);
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      let statuses = $("status");
      let status;
      // console.log(statuses);
      //console.log(statuses);
      // console.log(error.response);
      //
      statuses.each(() =>{
        status = $(this).text();
        console.log(status);
      });
      // console.log(error.response.data);
      // console.log(error.response.status);
      // console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
    }
    //console.log(error.config);
  });
