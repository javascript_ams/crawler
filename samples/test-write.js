const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
mongoose.connect("mongodb://localhost/webcrawler", { useMongoClient: true }); // get package

let Domain = require('../models/domains'); // get user model

let currentDate = new Date(); // save current date
let thedomains = [];
let saves = [];
let newDomain = new Domain({ // create a new domain
  protocol: 'https://',
  host: 'andev.ch',
  domain: 'andev.ch',
  url: 'https://andev.ch',
  country: 'CH',
  tld: 'ch',
  created: currentDate,
  lastCrawled: null,
  isUsed: false
});
let newDomain2 = new Domain({ // create a new domain
  protocol: 'http://',
  host: 'odnti.ch',
  domain: 'odnti.ch',
  url: 'http://odnti.ch',
  country: 'CH',
  tld: 'ch',
  created: currentDate,
  lastCrawled: null,
  isUsed: false
});
let newDomain3 = new Domain({ // create a new domain
  protocol: 'https://',
  host: 'www.wbf.admin.ch',
  domain: 'wbf.admin.ch',
  url: 'https://www.wbf.admin.ch',
  country: 'CH',
  tld: 'ch',
  created: currentDate,
  lastCrawled: null,
  isUsed: false
});
let newDomain4 = new Domain({ // create a new domain
  host: 'https://www.spamguard.to',
  country: 'CH',
  tld: 'ch',
  created: currentDate,
  lastCrawled: null,
  isUsed: false
});
let newDomain5 = new Domain({ // create a new domain
  host: 'https://www.andev.ch',
  country: 'CH',
  tld: 'ch',
  created: currentDate,
  lastCrawled: null,
  isUsed: false
});
let newDomain6 = new Domain({ // create a new domain
  host: 'https://www.ict-skills.ch',
  country: 'CH',
  tld: 'ch',
  created: currentDate,
  lastCrawled: null,
  isUsed: false
});
let removeProtocol = /(http:\/\/?|https:\/\/)((www)?)/;
// newDomain.save(function(error) { // save domain to db
//   if (error) {
//     throw error;
//   }
//   console.log('Domain %s created!', newDomain.host);
// });

saves.push(newDomain.save());
// thedomains.push(newDomain, newDomain2, newDomain3, newDomain4, newDomain5,newDomain6);
// thedomains.forEach(function(domain){
//   saves.push(saveit(domain));
// });
// function saveit(domain){
//   return new Promise((resolve, reject) =>{
//   console.log("loop");
//   host = domain.host.replace(removeProtocol, "");
//   console.log(host);
//   Domain.findOne({host: {$regex: host}}, function(err, success){
//     if (err){
//       console.log(error);
//     } else{
//       console.log(success);
//       if (success === null) {
//         domain.save(function(error) {
//           resolve(console.log(domain.host + " inserted"));
//         });
//
//       } else {
//         resolve(console.log(domain.host + " already present"));
//
//       }
//     }
//   });
// });
// }
// console.log(saves);

Promise.all(saves).then(() => {
  console.log(saves);
  console.log('All Domains saved');
  mongoose.connection.close();
}).catch(err => {
  console.log("duplicate");
  mongoose.connection.close();
});
