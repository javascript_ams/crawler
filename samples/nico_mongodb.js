let MongoClient = require('mongodb').MongoClient;
let assert = require('assert');
let ObjectId = require('mongodb').ObjectId;
let url = 'mongodb://localhost:27017/mydb';

let schreibeDokument = function(db, callback) {

  db.collection('meinErsteCollection').insertOne( {
    "name": {
      "vorname" : "Nico",
      "nachname" : "Gahlinger"
    },
    "geburtsdatum" : new Date("1992-01-01T00:00:00Z"),
    "adresse" : {
      "strasse" : "Keiahnigstr. 34",
      "ort" : "Winterthur",
      "plz" : 8400
    },
    "beruf" : "informatiker",
    "trinktGerne" : true
  }, function(err, result) {
    assert.equal(err, null);
    console.log("Dokument wurde zu meinErsteCollection hinzugefügt");
    callback();
  });
};

MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  schreibeDokument(db, function() {
    db.close;
  });
});
