var request = require("request");
var cheerio = require("cheerio");
var URI = "https://www.resign.ch/";
var pagesToVisit = [];
var visitedPages = new Set();
var linkCounter = 0;

crawl(URI);

function crawl(URI) {
  request(URI, function(error, response, body) {

    visitedPages.add(URI);  // diese URI zum Set hinzufügen, damit nicht zweimal die gleiche Seite beuscht wird
    linkCounter++;          // Seitenanzahl erhöhen
    var nextPage;

    /* Errorcatching */
    if (error) {
      console.log("ERROR: " + error);
    }
    /* Falls keine response vom Server kommt */
    if (response === undefined){
      console.log(URI + "antwortet nicht");
      nextPage = pagesToVisit.pop();
      /* Falls keine links mehr zur Verfügung stehen beende Prozess*/
      if (nextPage === undefined) {
        console.log("============================================");
        console.log("Keine Seiten mehr vorhanden. Beende Vorgang");
        console.log(`Es wurden ${linkCounter} Seiten durchforstet`);
        return;          // gehe raus aus prozess
      }
      crawl(nextPage);   // crawle neue seite
      return;            // gehe raus aus prozess
    }
    // Bei Antwort vom Server, schreibe URI und StatusCode */
    if (response.statusCode) {
      console.log(URI + " hat Status code: " + response.statusCode);
    }
    var $ = cheerio.load(body);
    var absoluteHref = $("a[href^='https']");

    absoluteHref.each(function() {
      var link = $(this).attr("href");
      // var text = link.text();
      // var href = link.attr("href");
      if (link.indexOf(URI) < 0) return;
      /* Pushe nur links ins Array die noch nicht besucht wurden */
      if (visitedPages.has(link) === false){
        pagesToVisit.push(link);
      }
    });

    nextPage = pagesToVisit.pop();
    /* Falls keine links mehr zur Verfügung stehen beende Prozess*/
    if (nextPage === undefined) {
      console.log("============================================");
      console.log("Keine Seiten mehr vorhanden. Beende Vorgang");
      console.log(`Es wurden ${linkCounter} Seiten durchforstet`);
      return;
    }
    crawl(nextPage);
    return;
  });
}
