const mongoose = require('mongoose');
mongoose.connect("mongodb://localhost/webcrawler", { useMongoClient: true }); // get package

const Domain = require('../models/domains');

// function getDomain() {
//   let promise = Domain.findOne({ "country": /^CH/}, 'host').exec();
//
//   return promise;
// }
//
// let url;
// let promise = getDomain();
// promise.then(function(domains){
//   console.log(domains.host);
//   // domains.forEach(function(domain) {
//   //   url = domain.host;
//   //   console.log(domain);
//   // });
//   mongoose.connection.close();
// });

// let query = Domain.find({ 'host': 'andev.ch'});
// query.exec(function(err,domains) {
//   console.log(domains[0].host);
// });

let id = "59f0595abb89921846de5d39";

function updateCrawlTime(id){
  let updateDate = new Date();
  console.log("updateTime DOMAINID: " + id);
  Domain.findByIdAndUpdate(id, { lastCrawled: updateDate}, function(error) {
    if (error) {console.log(error);}
    console.log("lastCrawled_at was updated");
    mongoose.connection.close();
  });
}

updateCrawlTime(id);
