const mongoose = require('mongoose');
const DBFile = require('../src/database');
const DB = DBFile();
mongoose.Promise = require('bluebird');
mongoose.connect("mongodb://localhost/webcrawler", {
  useMongoClient: true
});

let thisDate = new Date();
let domain =
{
  host: "http://de.wikipedia.org",
  country: "CH",
  tld: "ch"
};

// let promise = DB.getDomainPromise();
//
//
// promise.then((domains) => {
//   console.log(domains);
//   mongoose.connection.close();
// });

DB.saveDomains(domain, thisDate);
mongoose.connection.close();
