const cluster = require('cluster');
const now = require('performance-now');
const convert = require("./src/convert-time");
let numCPUs = require('os').cpus().length;

let manTimeStart;
let manTimeEnd;
let domainsCrawled = 0;
let noMoreDomainsToCrawl = false;
let domainsSaved = 0;
let worker;
let running = true;

function message_handler(msg)
{
  if (msg.noDomain) {
    if (noMoreDomainsToCrawl === false) {
      noMoreDomainsToCrawl = true;
      console.log("noMoreDomainsToCrawl: " + noMoreDomainsToCrawl);
    }
    console.log(msg.noDomain);
  }
  if (msg.finished) {
    console.log(msg.finished);
    domainsCrawled++;
  }
  if (msg.domainsSaved) {
    domainsSaved = msg.domainsSaved;
    console.log("DOMAINS SAVED: " + domainsSaved);
  }
}

if (cluster.isMaster) {
  manTimeStart = now();
  console.log(`Master ${process.pid} is running`);

  if (running && !noMoreDomainsToCrawl &&
    Object.keys(cluster.workers).length < numCPUs && domainsCrawled < 50) {
    for (let i = Object.keys(cluster.workers).length; i < numCPUs; i++) {
      worker = cluster.fork();
      worker.setMaxListeners(worker.getMaxListeners() + 1);
      worker.on('message', message_handler);
    }
  }

  cluster.on('exit', (slave, code, signal) => {

    slave.setMaxListeners(slave.getMaxListeners() - 1);
    slave.removeAllListeners();

    console.log(`worker ${slave.id} with ${slave.process.pid} died`);
    console.log("DOMAINS CRAWLED" + domainsCrawled);

    if (Object.keys(cluster.workers).length === 0 && domainsSaved > 0 && noMoreDomainsToCrawl) {
      noMoreDomainsToCrawl = false;
      domainsSaved = 0;
    }

    console.log("CRAWLERS: " + Object.keys(cluster.workers).length);
    console.log("NO MORE DOMAINS: " + noMoreDomainsToCrawl);

    if (running && !noMoreDomainsToCrawl &&
      Object.keys(cluster.workers).length < numCPUs && domainsCrawled < 50) {
      for (let i = Object.keys(cluster.workers).length; i < numCPUs; i++) {
        worker = cluster.fork();
        worker.setMaxListeners(worker.getMaxListeners() + 1);
        worker.on('message', message_handler);
      }
    }
    if (domainsCrawled >= 50) {
      manTimeEnd = now();
      console.log("*******************************************");
      console.log(`Limit reached. Crawled ${domainsCrawled} domains.`);
      console.log("Time elapsed: " + convert.timeFormat(manTimeEnd - manTimeStart));
      console.log('Ending process');
    }
  });

} else {
  console.log(`worker ${cluster.worker.id} with ${process.pid} started`);
  const crawler = require('./src/crawler');
}

// process.on('SIGINT', () => {
//   console.log('Received SIGINT');
//   running = false;
// });
