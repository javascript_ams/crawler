const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let relLinksSchema = new Schema ({
  relLinks: {
    count: Number,
    links: []
  }
});

let absLinksSchema = new Schema ({
  absLinks: {
    count: Number,
    links: [
      {
        link: String,
        dead: Boolean,
        hadError: Boolean
      }],
  }
});

let pageDetailSchema = new Schema ({
  page: String,
  status: Number,
  size: Number,
  relLinks: {
    type: {relLinksSchema},
    default: undefined
  },
  absLinks: {
    type: {absLinksSchema},
    default: undefined
  },
  noFollow: Boolean,
  noResponse: Boolean,
  hadError: Boolean
});

let filesSchema = new Schema({
  filesCounter: Number,
  filesCheckedCounter: Number,
  filesChecked: [
    {
    url: String,
    status: Number,
    dataType: String,
    size: Number,
    dead: Boolean,
    hadError: Boolean
  }]
});

let domainDetailSchema = new Schema({
  hasRobots: Boolean,
  hasSitemap: Boolean,
  pagesVisited: Number,
  pageDetail: {
    type: [pageDetailSchema],
    default: undefined
  },
  externalLinks: Number,
  internalLinks: Number,
  timeElapsed: Number,
  created: Date,
  tooManyErrors: Boolean,
  linkCount: [
    {
    link: String,
    count: Number
  }],
  files: {
    type: filesSchema,
    default: undefined
  },
  domain: {
    type: mongoose.Schema.Types.ObjectId,
    refs: 'Domain'
  },
  domainName: String
});

let DomainDetail = mongoose.model('Domain_Detail', domainDetailSchema);

module.exports = DomainDetail;
