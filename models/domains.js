const mongoose = require('mongoose');

let Schema = mongoose.Schema; // grab schema

let domainSchema = new Schema({ // create schema
  protocol: String,
  host : String,
  domain: {type: String, required: true, unique: true},
  url : String,
  country: String,
  tld: String,
  created: Date,
  lastCrawled: Date,
  isUsed: Boolean
});

let Domain = mongoose.model('Domain', domainSchema); // create model for use
module.exports = Domain;
